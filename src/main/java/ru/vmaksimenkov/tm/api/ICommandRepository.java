package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}