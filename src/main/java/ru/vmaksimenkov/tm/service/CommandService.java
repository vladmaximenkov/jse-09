package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.ICommandRepository;
import ru.vmaksimenkov.tm.api.ICommandService;
import ru.vmaksimenkov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
