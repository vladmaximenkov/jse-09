package ru.vmaksimenkov.tm.bootstrap;

import ru.vmaksimenkov.tm.api.ICommandController;
import ru.vmaksimenkov.tm.api.ICommandRepository;
import ru.vmaksimenkov.tm.api.ICommandService;
import ru.vmaksimenkov.tm.constant.ArgumentConst;
import ru.vmaksimenkov.tm.constant.TerminalConst;
import ru.vmaksimenkov.tm.controller.CommandController;
import ru.vmaksimenkov.tm.repository.CommandRepository;
import ru.vmaksimenkov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String... args) {
        commandController.showWelcome();
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command: ");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            default: incorrectCommand();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_EXIT: commandController.sysExit(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            default: incorrectCommand();
        }
    }

    public void incorrectCommand() {
        System.out.println("Unknown command. Type help to see all available commands");
    }

    public void incorrectArgument() {
        System.out.println("Unknown argument. Type -h to see all available arguments");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
